# Ansible | Usage du Yalm pour l'écriture des fichiers d'inventaire Ansible

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Dans ce lab, nous allons voir comment utiliser le format YAML pour réaliser la description d'un inventaire Ansible.

## Objectifs

- Nous allons modifier notre fichier hosts écrit au format INI pour qu'il soit au format YAML.

- Nous allons ensuite tester nos commandes ad-hoc avec ce nouveau format de fichier d'inventaire.

- Enfin, nous allons créer un fichier test3.txt sur les instances clientes depuis la machine hôte ansible

## Prérequis
Avoir une machine avec Ubuntu déjà installé.

Dans notre cas, nous allons provisionner une instance EC2 s'exécutant sous Ubuntu via AWS, sur laquelle nous effectuerons toutes nos configurations.

Documentation complémentaire.
[Documentation Ansible](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html)

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

## Réécriture du fichier hosts au format YAML

Pour rappel, notre fichier initial était écrit comme suit : 

```bash
[clients]
44.217.105.61 ansible_user=ubuntu ansible_ssh_private_key_file=.secret/devops-aCD.pem
```

Une fois réécrit au format YAML, on obtient :

```yaml
clients:
  hosts:
    44.217.105.61:
      ansible_user: ubuntu
      ansible_ssh_private_key_file: ".secret/devops-aCD.pem"
```

## Test de la commande ad-hoc "ping" avec ce nouveau format de fichier d'inventaire 
1. Ajout de la paire de clés SSH à la machine hôte

```bash
mkdir .secret
nano .secret/devops-aCD.pem
```
Une fois le fichier ouvert, on va y rajouter le contenu de la clé SSH générée lors de la création de l'instance cliente.

2. Ajout des autorisations à la clé SSH

```bash
chmod 600 .secret/devops-aCD.pem
```
>![alt text](img/image.png)
*Modification des autorisations*

3. Test de la commande ad-hoc **ping** sur le nouveau fichier d'inventaire

```bash
ansible -i hosts all -m ping
```

>![alt text](img/image-1.png)
*On obtient une réponse de l'instance cliente*

## Création d'un fichier sur les machines clientes depuis l'hôte

1. Ajout d'une seconde machine cliente à notre inventaire

Après le provisionnement d'une seconde machine cliente, nous allons modifier le fichier d'inventaire :

```yaml
clients:
  hosts:
    44.217.105.61:
      ansible_user: ubuntu
      ansible_ssh_private_key_file: ".secret/devops-aCD.pem"
    54.144.89.11:
      ansible_user: ubuntu
      ansible_ssh_private_key_file: ".secret/devops-aCD.pem"
```

2. Test de l'inventaire à l'aide de la commande ad-hoc "ping"

```bash
ansible -i hosts all -m ping
```

>![alt text](img/image-2.png)
*Test des instances clientes*

3. Création du fichier **test3.txt** avec pour contenu **"hello aCloud.Digital!"**

```bash
ansible -i hosts all -m copy -a "dest=/home/ubuntu/test3.txt content='hello aCloud.Digital'"
```

>![alt text](img/image-3.png)
*Création du fichier **test3.txt** sur les différentes machines clientes*

4. Vérification de la présence des fichiers depuis les machines clientes

Une fois connecté à chacune des machines clientes, nous listons les fichiers du répertoire /home/ubuntu

```bash
ssh -i devops-aCD.pem ubuntu@public_ip_client1
```

>![alt text](img/image-4.png)
*Le fichier **test3.txt** est bien créé ainsi que le contenu souhaité sur le client1*


>![alt text](img/image-5.png)
*Le fichier **test3.txt** est bien créé ainsi que le contenu souhaité sur le client2*